
### mgmtstp27-app
* Contributors: P.BADY, M.E. HEGI
* Date: 2014-12-02
* Revision: 2014-12-04
* Version: 0.2

### Licence
```
License: GPL version 2 or newer
Copyright (C) 2001-2014  Pierre Bady

This program is free software/document; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program/document is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```

### Examples of Dataset




#### Table S1 (Bady et al 2012)
* __Title:__ Clinical and methylation characteristics of the 68 samples from M-GBM dataset.
* __Description:__ The table contains methylation probability (MGMT-STP27 response) and predicted methylation status (STP27 class; U, unmethylated/ M, methylated) of MGMT promoter given by the model MGMT-STP27. The observed M-values of the 18 probes related to the MGMT promoter region and MSP results are given for each sample. Survival information, time (Time) and status (alive=0, death =1, missing value is equal to NA), is provided for primary GBM (PrGBM=1).

<!--html_preserve--><div class="shiny-input-panel">
<div class="shiny-flow-layout">
<div>
<a id="downloadNCH" class="btn shiny-download-link" href="" target="_blank">
<!--SHINY.SINGLETON[7196694b46f8e7c34171939351e3a70cdd3ccf40]-->
<!--/SHINY.SINGLETON[7196694b46f8e7c34171939351e3a70cdd3ccf40]-->
<i class="fa fa-download"></i>
Download
</a>
</div>
</div>
</div><!--/html_preserve-->


#### Table S4 (Bady et al 2012)
* __Title:__ Methylation characteristics of the 241 samples from TCGA-GBM and 74 TCGA GBM samples profiled on the Illumina HM-450K platform.
* __Description:__ The table contains methylation probability (STP27 response) and expected methylation status (STP27 class; U, unmethylated/ M, methylated) of MGMT promoter given by the model STP27. The CIMP classification is obtained by CTWC procedure and GBM expression subtype is based on a modified model from [7]. The both datasets (HM-27K and HM-450K) were identified by the variable called âdatasetâ.

<!--html_preserve--><div class="shiny-input-panel">
<div class="shiny-flow-layout">
<div>
<a id="downloadTCGA" class="btn shiny-download-link" href="" target="_blank">
<!--SHINY.SINGLETON[7196694b46f8e7c34171939351e3a70cdd3ccf40]-->
<!--/SHINY.SINGLETON[7196694b46f8e7c34171939351e3a70cdd3ccf40]-->
<i class="fa fa-download"></i>
Download
</a>
</div>
</div>
</div><!--/html_preserve-->


### References
* Bady, P., D. Sciuscio, A.-C. Diserens, J. Bloch, M. J. van den Bent, C. Marosi, P.-Y. Dietrich, M. Weller, L. Mariani, F. L. Heppner, D. R. McDonald, D. Lacombe, R. Stupp, M. Delorenzi, and M. E. Hegi. 2012. MGMT methylation analysis of glioblastoma on the Infinium methylation BeadChip identifies two distinct CpG regions associated with gene silencing and outcome, yielding a prediction model for comparisons across datasets, tumor grades, and CIMP-status. Acta Neuropathologica 124:547-560. doi: [10.1007/s00401-012-1016-2](http://dx.doi.org/10.1007%2Fs00401-012-1016-2).

* Brennan, Cameron W., Roel G. W. Verhaak, A. McKenna, B. Campos, H. Noushmehr, Sofie R. Salama, S. Zheng, D. Chakravarty, J. Z. Sanborn, Samuel H. Berman, R. Beroukhim, B. Bernard, C.-J. Wu, G. Genovese, I. Shmulevich, J. Barnholtz-Sloan, L. Zou, R. Vegesna, Sachet A. Shukla, G. Ciriello, W. K. Yung, W. Zhang, C. Sougnez, T. Mikkelsen, K. Aldape, Darell D. Bigner, Erwin G. Van Meir, M. Prados, A. Sloan, Keith L. Black, J. Eschbacher, G. Finocchiaro, W. Friedman, David W. Andrews, A. Guha, M. Iacocca, Brian P. O'Neill, G. Foltz, J. Myers, Daniel J. Weisenberger, R. Penny, R. Kucherlapati, Charles M. Perou, D. N. Hayes, R. Gibbs, M. Marra, Gordon B. Mills, E. Lander, P. Spellman, R. Wilson, C. Sander, J. Weinstein, M. Meyerson, S. Gabriel, Peter W. Laird, D. Haussler, G. Getz, and L. Chin. 2013. The Somatic Genomic Landscape of Glioblastoma. Cell 155:462-477.

* Noushmehr, H., D. Weisenberger, K. Diefes, H. Phillips, K. Pujara, B. Berman, F. Pan, C. Pelloski, E. Sulman, and K. Bhat. 2010. Identification of a CpG island methylator phenotype that defines a distinct subgroup of glioma. Cancer Cell 17:510 - 522.

* The Cancer Genome Atlas Research Network. 2008. Comprehensive genomic characterization defines human glioblastoma genes and core pathways. Nature 455:1061-1068.

* RStudio and Inc. (2014). shiny: Web Application Framework for R. R package version 0.10.2.1. http://CRAN.R-project.org/package=shiny

* van den Bent, M. J., L. Erdem-Eraslan, A. Idbaih, J. de Rooi, P. H. C. Eilers, W. G. M. Spliet, W. F. A. den Dunnen, C. Tijssen, P. Wesseling, P. A. E. Sillevis Smitt, J. M. Kros, T. Gorlia, and P. J. French. 2013. MGMT-STP27 Methylation Status as Predictive Marker for Response to PCV in Anaplastic Oligodendrogliomas and Oligoastrocytomas. A Report from EORTC Study 26951. Clinical Cancer Research 19:5513-5522.
  
### Acknowledgements
The results published here are in part based upon data generated by The Cancer TCGA Genome Atlas pilot project established by the NCI and NHGRI. Information about TCGA and the investigators and institutions WHO constitute the TCGA research network can be found at [http://cancergenome.nih.gov](http://cancergenome.nih.gov). The dbGaP accession number to the specific version of the TCGA data set is phs000178.v8.p7.
