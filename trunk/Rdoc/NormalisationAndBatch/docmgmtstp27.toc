\select@language {french}
\contentsline {section}{\numberline {1}Motivations}{2}{section.1}
\contentsline {section}{\numberline {2}Data}{2}{section.2}
\contentsline {section}{\numberline {3}Preprocessing and Normalization}{2}{section.3}
\contentsline {section}{\numberline {4}Normalisation effect on the prediction}{3}{section.4}
\contentsline {section}{\numberline {5}Comparison of the three datasets (PP-27K, PP-450K, TCGA-450K)}{3}{section.5}
\contentsline {section}{\numberline {6}Normalization effect for the training dataset (M-GBM, \cite {Bady2012})}{7}{section.6}
\contentsline {section}{\numberline {7}Conclusion}{9}{section.7}
\contentsline {section}{\numberline {8}Acknowledgments}{9}{section.8}
\contentsline {section}{\numberline {9}Appendix}{10}{section.9}
\contentsline {subsection}{\numberline {9.1}Import raw HM-27K data (format .IDAT)}{10}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Session}{10}{subsection.9.2}
