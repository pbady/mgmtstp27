\select@language {french}
\contentsline {section}{\numberline {1}Motivations}{2}{section.1}
\contentsline {section}{\numberline {2}Data}{2}{section.2}
\contentsline {section}{\numberline {3}Probability that MGMT pomoter is methylated}{2}{section.3}
\contentsline {section}{\numberline {4}DNA methylation state of MGMT promoter}{3}{section.4}
\contentsline {section}{\numberline {5}Quality control for dataset prediction}{5}{section.5}
\contentsline {section}{\numberline {6}Quality control for single sample prediction}{7}{section.6}
\contentsline {section}{\numberline {7}Uncertainty and reliability of the model}{9}{section.7}
\contentsline {subsection}{\numberline {7.1}Extreme values}{9}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Confidence and tolerance intervals}{11}{subsection.7.2}
\contentsline {section}{\numberline {8}Acknowledgments}{11}{section.8}
\contentsline {section}{\numberline {9}Appendix}{12}{section.9}
\contentsline {subsection}{\numberline {9.1}Import raw HM-27K data (format .IDAT)}{12}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Session}{12}{subsection.9.2}
